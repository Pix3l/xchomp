# Makefile for Xchomp
#
CC= gcc
CFLAGS= -O2 -fomit-frame-pointer -fno-defer-pop -DFRAME_DELAY=20000 -g

LDLIBS= -lX11

OBJS= contact.o demo.o drivers.o main.o maze.o props.o resources.o status.o

all: xchomp

xchomp: $(OBJS)
	gcc $(OBJS) $(LDLIBS) -o $@

clean:
	rm *.o xchomp

.c.o: 
	$(CC) $(CFLAGS) $(DEFINES) -c -o $*.o $*.c
